package com.afs.tdd;

public class Move {
    public Position move(Position currentPosition) {
        int x = currentPosition.getxCoordinate();
        int y = currentPosition.getyCoordinate();
        Direction direction = currentPosition.getDirection();

        if (direction == Direction.N) {
            y = y + 1;
        }

        if (direction == Direction.E) {
            x = x + 1;
        }

        if (direction == Direction.S) {
            y = y - 1;
        }

        if (direction == Direction.W) {
            x = x - 1;
        }

        return  new Position(x, y, direction);
    }
}
