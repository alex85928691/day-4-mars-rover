package com.afs.tdd;

public class MoveControl {

    public String setMove(String order, Position position) {
        Position newPost = position;
        Move move = new Move();
        TurnLeft turnleft = new TurnLeft();
        TurnRight turnright = new TurnRight();


        for (char command : order.toCharArray()){
            if (command == 'M') {
                newPost = move.move(newPost);
            } else if (command == 'L') {
                newPost = turnleft.turn(newPost);
            } else if (command == 'R') {
                newPost = turnright.turn(newPost);
            }
        }

        return String.format("(%d,%d,%s)", newPost.getxCoordinate(), newPost.getyCoordinate(), newPost.getDirection());
    }
}
