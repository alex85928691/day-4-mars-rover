package com.afs.tdd;

import java.util.Objects;

public class Position {
    private int xCoordinate;
    private int yCoordinate;

    private Direction direction;

    public Position(int xCoordinate, int yCoordinate, Direction direction) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.direction = direction;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public Direction getDirection() {
        return direction;
    }


}