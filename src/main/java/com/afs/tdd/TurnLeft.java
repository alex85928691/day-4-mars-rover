package com.afs.tdd;

public class TurnLeft {

    public Position turn(Position position) {
        int x = position.getxCoordinate();
        int y = position.getyCoordinate();
        Direction direction = position.getDirection();

        if(direction == Direction.N){
            direction = Direction.W;

        }else if(direction == Direction.S){
            direction = Direction.E;

        }else if(direction == Direction.E){
            direction = Direction.N;

        }else if(direction == Direction.W){
            direction = Direction.S;
        }

        return new Position(x,y,direction);
    }
}
