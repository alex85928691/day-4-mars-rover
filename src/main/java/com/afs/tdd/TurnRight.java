package com.afs.tdd;

public class TurnRight {
    public Position turn(Position position) {
        int x = position.getxCoordinate();
        int y = position.getyCoordinate();
        Direction direction = position.getDirection();

        if(direction == Direction.N){
            direction = Direction.E;

        }else if(direction == Direction.W){

            direction = Direction.N;

        } else if(direction == Direction.S){
            direction = Direction.W;

        }else if(direction == Direction.E){
            direction = Direction.S;
        }

        return new Position(x,y,direction);
    }
}
