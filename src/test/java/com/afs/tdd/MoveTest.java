package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveTest {


    @Test
    void should_move_to_position_0_1_N_when_postion_is_0_0_N(){


        Move movement = new Move();

        Position position = new Position(0,0,Direction.N);

        Position  actual = movement.move(position);

        assertEquals(0, actual.getxCoordinate());
        assertEquals(1, actual.getyCoordinate());
        assertEquals(Direction.N, actual.getDirection());
    }
    @Test
    void should_move_to_position_1_0_E_when_postion_is_0_0_E(){


        Move movement = new Move();

        Position position = new Position(0,0,Direction.E);

        Position actual = movement.move(position);

        assertEquals(1, actual.getxCoordinate());
        assertEquals(0, actual.getyCoordinate());
        assertEquals(Direction.E, actual.getDirection());
    }

    @Test
    void should_move_to_position_0_negative1_E_when_postion_is_0_0_S(){


        Move movement = new Move();

        Position position = new Position(0,0,Direction.S);

        Position actual = movement.move(position);

        assertEquals(0, actual.getxCoordinate());
        assertEquals(-1, actual.getyCoordinate());
        assertEquals(Direction.S, actual.getDirection());
    }

    @Test
    void should_move_to_position_negative1_0_W_when_postion_is_0_0_W(){


        Move movement = new Move();

        Position position = new Position(0,0,Direction.W);

        Position actual = movement.move(position);

        assertEquals(-1, actual.getxCoordinate());
        assertEquals(0, actual.getyCoordinate());
        assertEquals(Direction.W, actual.getDirection());
    }


}
