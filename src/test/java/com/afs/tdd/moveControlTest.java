package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class moveControlTest {


    @Test
    void should_return_string_when_setMove_input_M_and_position(){
        MoveControl movecontrol = new MoveControl();

        Position position = new Position(0,0,Direction.N);

        String actual = movecontrol.setMove("M",position);

        assertEquals("(0,1,N)",actual);

    }

    @Test
    void should_return_string_when_setMove_input_R_and_position(){
        MoveControl movecontrol = new MoveControl();

        Position position = new Position(0,0,Direction.N);

        String actual = movecontrol.setMove("R",position);

        assertEquals("(0,0,E)",actual);

    }

    @Test
    void should_return_string_when_setMove_input_L_and_position(){
        MoveControl movecontrol = new MoveControl();

        Position position = new Position(0,0,Direction.N);

        String actual = movecontrol.setMove("L",position);

        assertEquals("(0,0,W)",actual);

    }

    @Test
    void should_return_string_when_setMove_input_MM_and_position(){
        MoveControl movecontrol = new MoveControl();

        Position position = new Position(0,0,Direction.N);

        String actual = movecontrol.setMove("MM",position);

        assertEquals("(0,2,N)",actual);

    }

    @Test
    void should_return_string_when_setMove_input_MR_and_position(){
        MoveControl movecontrol = new MoveControl();

        Position position = new Position(0,0,Direction.N);

        String actual = movecontrol.setMove("MR",position);

        assertEquals("(0,1,E)",actual);

    }

    @Test
    void should_return_string_when_setMove_input_ML_and_position(){
        MoveControl movecontrol = new MoveControl();

        Position position = new Position(0,0,Direction.N);

        String actual = movecontrol.setMove("ML",position);

        assertEquals("(0,1,W)",actual);

    }

    @Test
    void should_return_string_when_setMove_input_FLLRF_and_position(){
        MoveControl movecontrol = new MoveControl();

        Position position = new Position(0,0,Direction.N);

        String actual = movecontrol.setMove("MLLRM",position);

        assertEquals("(-1,1,W)",actual);

    }

}
