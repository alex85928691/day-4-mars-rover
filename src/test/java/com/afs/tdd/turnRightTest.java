package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class turnRightTest {
    @Test
    void should_move_to_position_0_0_E_when_postion_is_0_0_N(){


        TurnRight right = new TurnRight();

        Position position = new Position(0,0,Direction.N);

        Position actual = right.turn(position);

        assertEquals(0, actual.getxCoordinate());
        assertEquals(0, actual.getyCoordinate());
        assertEquals(Direction.E, actual.getDirection());
    }

    @Test
    void should_move_to_position_0_0_N_when_postion_is_0_0_W(){


        TurnRight right = new TurnRight();

        Position position = new Position(0,0,Direction.W);

        Position actual = right.turn(position);

        assertEquals(0, actual.getxCoordinate());
        assertEquals(0, actual.getyCoordinate());
        assertEquals(Direction.N, actual.getDirection());
    }

    @Test
    void should_move_to_position_0_0_W_when_postion_is_0_0_S(){


        TurnRight right = new TurnRight();

        Position position = new Position(0,0,Direction.S);

        Position actual = right.turn(position);

        assertEquals(0, actual.getxCoordinate());
        assertEquals(0, actual.getyCoordinate());
        assertEquals(Direction.W, actual.getDirection());
    }

    @Test
    void should_move_to_position_0_0_S_when_postion_is_0_0_E(){


        TurnRight right = new TurnRight();

        Position position = new Position(0,0,Direction.E);

        Position actual = right.turn(position);

        assertEquals(0, actual.getxCoordinate());
        assertEquals(0, actual.getyCoordinate());
        assertEquals(Direction.S, actual.getDirection());
    }




}
